function [GR]= gear_ratio(Trans,Gear)
% outputs overall gear reduction (including primary and final) for given
% gear

% Initiate gear variable
GR=0;

gr_num= length(Trans)-4;    % number of gears

for i= 1:gr_num
    if Gear==i
        GR= Trans(i)*Trans(length(Trans)-3)*Trans(length(Trans)-2);
    else
    end
end

end