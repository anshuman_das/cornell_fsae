function [Fxy] = polyforces(Coeff,Fz)
% Calculates lateral or longitudinal tire forces given polynomial tire
% model and normal load
% Y = P(1)*X^N + P(2)*X^(N-1) + ... + P(N)*X + P(N+1)

N= length(Coeff)-1;     % Nth degree polynomial

for i= 1:N
    F(i)= Coeff(i)*Fz^(N-i+1);
end

Fxy= sum(F(i))+Coeff(N+1);

end