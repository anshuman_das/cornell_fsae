function [Vy_max,Ay,Ayf,Ayr,Skid,Loads_tot,Loads_WT,Loads_Aero]= corner_max_load(RL,C,A,Tir)
% Calculates maximum cornering speeds from given racing line and car
% specifications

% Extract car specs from excel files
% General Specs
m= C(1);            % Car mass (kg)
m_driver= C(2);     % Driver mass (kg)
M= m+m_driver;      % Total mass (kg)
L= C(3);            % Wheel base (m)
tf= C(4);           % Front track width (m)
tr= C(5);           % Rear track width (m)
Hcg= C(6);          % CG height (m)
rwd= C(7);          % Rear weight distribution
fwd= 1-rwd;         % Front weight distribution
Hrf= C(8);          % Front roll center height (m)
Hrr= C(9);          % Rear roll center height (m)
Kf= C(10);          % Front roll stiffness (N*m/deg)
Kr= C(11);          % Rear roll stiffness (N*m/deg)

% Aero
S_body= A(1);       % Frontal area of car body including tires (m^2)
CL_body= A(2);      % Car body lift coefficient
CD_body= A(3);      % Car body drag coefficient
S_fw= A(4);         % Front wing area (m^2)
CL_fw= A(5);        % Front wing lift coefficient
CD_fw= A(6);        % Front wing drag coefficient
x_fw= A(7);         % x location of front wing center of pressure (m)
z_fw= A(8);         % z location of front wing center of pressure (m)
S_rw= A(9);         % Rear wing area (m^2)
CL_rw= A(10);       % Rear wing lift coefficient
CD_rw= A(11);       % Rear wing drag coefficient
x_rw= -A(12);       % x location of rear wing center of pressure (m)
z_rw= A(13);        % z location of rear wing center of pressure (m)

% Tires
Fy_c= Tir(1,:);     % friction function

g= 9.81;            % gravity (m/s^2)
rho= 1.225;         % air density (kg/m^3)

% Vector containing radii of the centerline of track
% Straights have radii of Inf
r= abs(RL(:,2)./RL(:,1));
% Initiate max cornering speed vector
Vy_max= zeros(length(RL(:,1)),1);
% Initiate lateral acceleration vectors
Ay= zeros(length(RL(:,1)),1);    % vehicle acceleration (m/s^2)
Ayf= zeros(length(RL(:,1)),1);   % front acceleration (m/s^2)
Ayr= zeros(length(RL(:,1)),1);   % rear acceleration (m/s^2)

Fzfr_WT = zeros(length(RL(:,1)),1); % normal load front right by weight tramsfer
Fzfr_Aero = zeros(length(RL(:,1)),1); % normal aero load front right 
Fzfr_tot = zeros(length(RL(:,1)),1); % total normal load front right

Fzfl_WT = zeros(length(RL(:,1)),1); % normal load front left by weight tramsfer
Fzfl_Aero = zeros(length(RL(:,1)),1); % normal aero load front left 
Fzfl_tot = zeros(length(RL(:,1)),1); % total normal load front left

Fzrr_WT = zeros(length(RL(:,1)),1); % normal load rear right by weight tramsfer
Fzrr_Aero = zeros(length(RL(:,1)),1); % normal aero load rear right 
Fzrr_tot = zeros(length(RL(:,1)),1); % total normal load rear right

Fzrl_WT = zeros(length(RL(:,1)),1); % normal load rear left by weight tramsfer
Fzrl_Aero = zeros(length(RL(:,1)),1); % normal aero rear left right 
Fzrl_tot = zeros(length(RL(:,1)),1); % total normal rear left right

for i= 1:length(RL(:,1))
    a= find(r(i)==r(1:i-1),1);  % checks to see if any corner radii are the same
    if r(i)==Inf                % straight line case
        
        Vy_max(i)= inf;

        Fzfr_WT(i) = inf;
        Fzfr_Aero(i) = inf;
        Fzfr_tot(i) = inf;

        Fzfl_WT(i) = inf;
        Fzfl_Aero(i) = inf;
        Fzfl_tot(i) = inf;

        Fzrr_WT(i) = inf;
        Fzrr_Aero(i) = inf;
        Fzrr_tot(i) = inf;

        Fzrl_WT(i) = inf;
        Fzrl_Aero(i) = inf;
        Fzrl_tot(i) = inf;
    elseif ~isempty(a)          % if the corner radius is the same as a 
        % previously solved radius, uses previous solution
        Vy_max(i)= Vy_max(a);
        Ay(i)= Ay(a);
        Ayf(i)= Ayf(a);
        Ayr(i)= Ayr(a);
        
        Fzfr_WT(i) = Fzfr_WT(a);
        Fzfr_Aero(i) = Fzfr_Aero(a);
        Fzfr_tot(i) = Fzfr_tot(a);

        Fzfl_WT(i) = Fzfl_WT(a);
        Fzfl_Aero(i) = Fzfl_Aero(a);
        Fzfl_tot(i) = Fzfl_tot(a);

        Fzrr_WT(i) = Fzrr_WT(a);
        Fzrr_Aero(i) = Fzrr_Aero(a);
        Fzrr_tot(i) = Fzrr_tot(a);

        Fzrl_WT(i) = Fzrl_WT(a);
        Fzrl_Aero(i) = Fzrl_Aero(a);
        Fzrl_tot(i) = Fzrl_tot(a);
    else
        Fzfr= 0.5*fwd*M*g;     % Front right static normal load (N)
        Fzfl= 0.5*fwd*M*g;     % Front left static normal load (N)
        Fzrr= 0.5*rwd*M*g;     % Rear right static normal load (N)
        Fzrl= 0.5*rwd*M*g;     % Rear left static normal load (N)
        
        % solve as if every corner is a left turn
        % loop to converge on a solution
        for j= 1:20
            Fyfr= abs(polyval(Fy_c,abs(Fzfr)));   % Front right lateral force (N)
            if Fzfl<0           % wheel lift
                Fyfl= 0;
            else
                Fyfl= abs(polyval(Fy_c,abs(Fzfl)));% Front left lateral force (N)
            end
            Fyrr= abs(polyval(Fy_c,abs(Fzrr)));    % Rear right lateral force (N)
            if Fzrl<0           % wheel lift
                Fyrl= 0;
            else
                Fyrl= abs(polyval(Fy_c,abs(Fzrl)));% Rear left lateral force (N)
            end
            
            Ayf(i)= (Fyfr+Fyfl)/(M*fwd);    % front lateral acceleration (m/s^2)
            Ayr(i)= (Fyrr+Fyrl)/(M*rwd);    % rear lateral acceleration (m/s^2)
            Ay(i)= min(Ayf(i),Ayr(i));      % vehicle lateral acceleration (m/s^2)
            
            Vy_max(i)= sqrt(Ay(i)*r(i));     % cornering speed (m/s)
            
            % front lateral weight transfer (N)
            lat_wt_trans_f= Ay(i)*M/tf*(fwd*Hrf+Kf*(Hcg-Hrf)/(Kf+Kr));
            % rear lateral weight transfer (N)
            lat_wt_trans_r= Ay(i)*M/tr*(rwd*Hrr+Kr*(Hcg-Hrr)/(Kf+Kr));
            
            % Aero loads
            q= 0.5*rho*(Vy_max(i))^2;       % dynamic pressure (pa)
            L_body= q*S_body*CL_body;       % body lift force (N)
            D_body= q*S_body*CD_body;       % body drag force (N)
            L_fw= q*S_fw*CL_fw;             % front wing lift force (N)
            D_fw= q*S_fw*CD_fw;             % front wing drag force (N)
            L_rw= q*S_rw*CL_rw;             % rear wing lift force (N)
            D_rw= q*S_rw*CD_rw;             % rear wing drag force (N)
            
            % Normal load on rear tires
            Fzr= (L_fw*x_fw+D_fw*z_fw+D_body*Hcg+rwd*M*g*L+D_rw*z_rw...
                -L_body*rwd*L-L_rw*(x_rw+L))/L;
            % Normal load on front tires
            Fzf= M*g-L_fw-L_rw-L_body-Fzr;
            
            Fzfr = (0.5*Fzf+lat_wt_trans_f);        % Front right normal load (N)
            Fzfl = (0.5*Fzf-lat_wt_trans_f);        % Front left normal load (N)
            Fzrr = (0.5*Fzr+lat_wt_trans_r);        % Rear right normal load (N)
            Fzrl = (0.5*Fzr-lat_wt_trans_r);        % Rear left normal load (N)
            
        end

            
            Fzfr_tot(i)= Fzfr;        % Front right normal load (N)
            Fzfl_tot(i)= Fzfl;        % Front left normal load (N)
            Fzrr_tot(i)= Fzrr;        % Rear right normal load (N)
            Fzrl_tot(i)= Fzrl;        % Rear left normal load (N)
            
            Fzfr_WT(i)=  0.5*fwd*M*g+lat_wt_trans_f;        % Front right weight transfer load (N)
            Fzfl_WT(i)=  0.5*fwd*M*g-lat_wt_trans_f;        % Front left weight transfer load (N)
            Fzrr_WT(i)= 0.5*rwd*M*g+lat_wt_trans_r;        % Rear right weight transfer load (N)
            Fzrl_WT(i)= 0.5*rwd*M*g-lat_wt_trans_r;        % Rear left weight transfer load (N)
            
            Fzfr_Aero(i)= Fzfr_tot(i)-Fzfr_WT(i);        % Front right normal load (N)
            Fzfl_Aero(i)= Fzfl_tot(i)-Fzfl_WT(i);        % Front left normal load (N)
            Fzrr_Aero(i)= Fzrr_tot(i)-Fzrr_WT(i);        % Rear right normal load (N)
            Fzrl_Aero(i)= Fzrl_tot(i)-Fzrl_WT(i);        % Rear left normal load (N)
    end
end

Loads_tot (:,1) = Fzfr_tot./g;  % Total loads in kg
Loads_tot (:,2) = Fzfl_tot./g;
Loads_tot (:,3) = Fzrr_tot./g;
Loads_tot (:,4) = Fzrl_tot./g;

Loads_WT (:,1) = Fzfr_WT./g;    % weight transfer loads in kg
Loads_WT (:,2) = Fzfl_WT./g;
Loads_WT (:,3) = Fzrr_WT./g;
Loads_WT (:,4) = Fzrl_WT./g;

Loads_Aero (:,1) = Fzfr_Aero./g;    % Aero loads in kg
Loads_Aero (:,2) = Fzfl_Aero./g; 
Loads_Aero (:,3) = Fzrr_Aero./g;
Loads_Aero (:,4) = Fzrl_Aero./g;

% Solve for skidpad time, velocity, lateral acceleration

% Set skidpad radius (m)
r_skid= 9.125;

% Initiate skidpad vector
Skid= zeros(3,1);       % 1=time, 2=velocity, 3=acceleration

Fzfr= 0.5*fwd*M*g;      % Front right static normal load (N)
Fzfl= 0.5*fwd*M*g;      % Front left static normal load (N)
Fzrr= 0.5*rwd*M*g;      % Rear right static normal load (N)
Fzrl= 0.5*rwd*M*g;      % Rear left static normal load (N)

for j= 1:20
    Fyfr= abs(polyval(Fy_c,abs(Fzfr)));   % Front right lateral force (N)
    if Fzfl<0           % wheel lift
        Fyfl= 0;
    else
        Fyfl= abs(polyval(Fy_c,abs(Fzfl)));% Front left lateral force (N)
    end
    Fyrr= abs(polyval(Fy_c,abs(Fzrr)));    % Rear right lateral force (N)
    if Fzrl<0           % wheel lift
        Fyrl= 0;
    else
        Fyrl= abs(polyval(Fy_c,abs(Fzrl)));% Rear left lateral force (N)
    end
            
    Ayf_s= (Fyfr+Fyfl)/(M*fwd);     % front lateral acceleration (m/s^2)
    Ayr_s= (Fyrr+Fyrl)/(M*rwd);     % rear lateral acceleration (m/s^2)
    Skid(3)= min(Ayf_s,Ayr_s);      % vehicle lateral acceleration (m/s^2)
    
    Skid(2)= sqrt(Skid(3)*r_skid);     % cornering speed (m/s)
            
    % front lateral weight transfer (N)
    lat_wt_trans_f= Skid(3)*M/tf*(fwd*Hrf+Kf*(Hcg-Hrf)/(Kf+Kr));
    % rear lateral weight transfer (N)
    lat_wt_trans_r= Skid(3)*M/tr*(rwd*Hrr+Kr*(Hcg-Hrr)/(Kf+Kr));
           
    % Aero loads
    q= 0.5*rho*(Skid(2))^2;         % dynamic pressure (pa)
    L_body= q*S_body*CL_body;       % body lift force (N)
    D_body= q*S_body*CD_body;       % body drag force (N)
    L_fw= q*S_fw*CL_fw;             % front wing lift force (N)
    D_fw= q*S_fw*CD_fw;             % front wing drag force (N)
    L_rw= q*S_rw*CL_rw;             % rear wing lift force (N)
    D_rw= q*S_rw*CD_rw;             % rear wing drag force (N)
          
    % Normal load on rear tires
    Fzr= (L_fw*x_fw+D_fw*z_fw+D_body*Hcg+rwd*M*g*L+D_rw*z_rw...
        -L_body*rwd*L-L_rw*(x_rw+L))/L;
    % Normal load on front tires
    Fzf= M*g-L_fw-L_rw-L_body-Fzr;
            
    Fzfr= (0.5*Fzf+lat_wt_trans_f);        % Front right normal load
    Fzfl= (0.5*Fzf-lat_wt_trans_f);        % Front left normal load
    Fzrr= (0.5*Fzr+lat_wt_trans_r);        % Rear right normal load
    Fzrl= (0.5*Fzr-lat_wt_trans_r);        % Rear left normal load
end

Skid(1)= 2*pi()*r_skid/Skid(2);
Vy_max_kmh = Vy_max.*3.6;

figure (3)
hold on
title ('Aero loads on tires during cornering')
xlabel('corner max speed (km/h)')
ylabel('Aero down force (kg)')
plot (Vy_max_kmh,Loads_Aero(:,1),'k*')
plot (Vy_max_kmh,Loads_Aero(:,3),'r*')
legend ('Front','Rear','Location','Best')

figure (4)
hold on
title ('Total loads on tires during cornering')
xlabel('corner max speed (km/h)')
ylabel('Total load on tires (kg)')
plot (Vy_max_kmh,Loads_tot(:,1),'k*')
plot (Vy_max_kmh,Loads_tot(:,2),'b*')
plot (Vy_max_kmh,Loads_tot(:,3),'ro')
plot (Vy_max_kmh,Loads_tot(:,4),'go')
legend ('Front Right','Front left','Rear Right','Rear Left','Location','Best')
end



