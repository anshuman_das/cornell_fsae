function LapSim_Endo_ver_2()
% Metric Units

close all
clc

% Set time interval of calculation (resolution) (sec)
t_step= 0.001;
% Set initial speed (0 m/s for launch) (m/s)
v_i= 0;

% Input car specification files
Chassis= 'Stock Car Chassis.xlsx';      % Excel file of general/chassis specs
[Chassis,~,~]= xlsread(Chassis);    % Extract specs from excel file
Aero= 'Stock Car (No) Aero.xlsx';            % Excel file of aero specs
[Aero,~,~]= xlsread(Aero);          % Extract specs from excel file
Tire_dim= 'ARG13 Tire Dim.xlsx';    % Excel file of tire dimensions
[Tire_dim,~,~]= xlsread(Tire_dim);  % Extract dimensions from excel file
Torque= 'ARG13 Torque Curve.xlsx';  % Excel file of torque curve
[Torque,~,~]= xlsread(Torque);      % Extract values from excel file
Trans= 'ARG13 Transmission.xlsx';   % Excel file of transmission specs
[Trans,~,~]= xlsread(Trans);        % Extract specs from excel file

% Tire Model
% Excel file of tire model (ex. polyfit, pacejka)
Tire_model= 'Hoosier 18x6-10 12psi_polyfit.xlsx';
% Extract values from excel file
[Tire_model,~,~]= xlsread(Tire_model);

% Input Track file
track= 'MIS Autox 2013.xlsx';            % Excel file of track dimensions
% Extract track dimensions from excel file
[T,~,~]= xlsread(track);

% Drive on the line provided or calculate racing line?
% 0= Use line provided
% 1= Calculate racing line
line_Y= 0;

if line_Y==0
    T= track_gen(track,0);          % Call track generation function
    RL= T;                          % Racing line is the provided line 
elseif line_Y==1
    WoT= T(1,4);                    % Specify width of track (m)
    T= track_gen(track,WoT);        % Call track generation function
    tf= Chassis(4);                 % Front track width (m)
    tr= Chassis(5);                 % Rear track width (m)
    twf= Tire_dim(3);               % Front tire width (m)
	twr= Tire_dim(4);               % Rear tire width (m)
    % Calculate Racing Line
    RL= raceline(T,WoT,max(tf+twf,tr+twr));
end

% Solve for Maximum Corner Speeds
[Vy_max,Ay,Ayf,Ayr,Skid]= corner_max(RL,Chassis,Aero,Tire_model);

% Solve for Straight Line Acceleration & Velocity
[x_b,Vx_b,Ax_b,x_a,Vx_a,Ax_a,Fz_b]= straight_line(RL,...
    Chassis,Aero,Tire_model,Tire_dim,Torque,Trans,t_step);

% Solve for times in each section of lap (s)
[t,tc,ts]= Time(RL,Vy_max,Vx_b,Vx_a,x_b,x_a,t_step,v_i);

% Lap Time (s)
T= sum(t);

% Accel Time (75m straight) (s)
t_accel= find(x_a >= 75,1)/1000;

disp(track)
disp('Lap Time (s)')
disp(T)
disp('Accel Time (s)')
disp(t_accel)
disp('Skidpad Time (s)')
disp(Skid(1))
disp('Section Times (s)')
disp(t)
disp('Time Spent in Corners (s)')
disp(sum(tc))
disp('Time Spent on Straights (s)')
disp(sum(ts))

end