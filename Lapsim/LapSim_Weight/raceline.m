function [RL]= raceline(T,WoT,cw)
% Calculates racing line dimensions from given track and car width

% Vector containing radii of the centerline of track
% Straights have radii of Inf
r= abs(T(:,2)./T(:,1));

% Calculate radii of the racing line
R= zeros(length(r),1);      % Initiate radius vector
for i= 1:length(R)
    % Straight section case: modification to radius
    if r(i)==Inf
        R(i)= T(i,2);
    % First track element is a corner and is followed by a straight
    elseif i==1 && r(i)~=0 && r(i+1)==Inf
        R(i)= r(i)+(WoT-cw)/2*(1+cos(T(i,3)*pi()/360))./(1-cos(T(i,3)*pi()/360));
    % First track element is a corner and is followed by a corner
    elseif i==1 && r(i)~=Inf && r(i+1)~=Inf
        % R(i)= 1;
    elseif i==length(R) && r(i)==Inf
        R(i)= T(i,2);
    elseif i==length(R) && r(i)~=Inf
        R(i)= r(i)+(WoT-cw)/2*(1+cos(T(i,3)*pi()/360))./(1-cos(T(i,3)*pi()/360)); 
    % Corner section is before and after a straight
    % Maximize corner radius
    elseif r(i)~=Inf && r(i+1)==Inf && r(i-1)==Inf
        R(i)= r(i)+(WoT-cw)/2*(1+cos(T(i,3)*pi()/360))./(1-cos(T(i,3)*pi()/360));
    % Lane Change: 2 successive corners in opposite directions
    % Straights before and after lane change
    % Both corners have same radius & angular change
    % Angular change must be <= 90deg
    elseif r(i)~=Inf && T(i,1)*T(i+1,1)==-1 && r(i-1)==Inf && r(i+2)==Inf
        if T(i-2,1)*T(i,1)==1
            R(i)= T(i,2);
        else
            ri= r(i)-(WoT-cw)/2;
            x= sqrt(r(i)^2+ri^2-2*r(i)*ri*cos(T(i,3)*pi()/180));
            theta= pi()/2-asin(r(i)*sin(T(i,3)*pi()/180)/x);
            R(i)= (WoT-cw)/(0.5*theta);
        end
        if T(i+3,1)*T(i+1,1)==1
            R(i+1)= T(i+1,2);
        else
            ri= r(i+1)-(WoT-cw)/2;
            x= sqrt(r(i+1)^2+ri^2-2*r(i+1)*ri*cos(T(i,3)*pi()/180));
            theta= pi()/2-asin(r(i+1)*sin(T(i+1,3)*pi()/180)/x);
            R(i+1)= (WoT-cw)/(0.5*theta);
        end
    end
end

RL= [T(:,1),R,T(:,3)];

% For Plotting:

seg_step= 50;      % Number of line segments per turn/straight (resolution)

% Racing Line coordinates
RL_cl= zeros(seg_step*length(RL)+1,3);  % Initialize matrix of coordinates
for i= 1:length(RL)
    for j= 1:seg_step
        if RL(i)==0          % straight case
            RL_cl((i-1)*seg_step+j+1,1)= RL_cl((i-1)*seg_step+j,1)+...
                RL(i,2)/seg_step*cos(RL_cl((i-1)*seg_step+j,3)*pi()/180);
            RL_cl((i-1)*seg_step+j+1,2)= RL_cl((i-1)*seg_step+j,2)+...
                RL(i,2)/seg_step*sin(RL_cl((i-1)*seg_step+j,3)*pi()/180);
            RL_cl((i-1)*seg_step+j+1,3)= RL_cl((i-1)*seg_step+j,3);
        elseif RL(i)==1      % left turn case
            RL_cl((i-1)*seg_step+j+1,1)= RL_cl((i-1)*seg_step+j,1)+...
                RL(i,2)*sqrt(2-2*cos(RL(i,3)/seg_step*pi()/180))*...
                cos((RL_cl((i-1)*seg_step+j,3)+RL(i,3)/seg_step/2)*pi()/180);
            RL_cl((i-1)*seg_step+j+1,2)= RL_cl((i-1)*seg_step+j,2)+...
                RL(i,2)*sqrt(2-2*cos(RL(i,3)/seg_step*pi()/180))*...
                sin((RL_cl((i-1)*seg_step+j,3)+RL(i,3)/seg_step/2)*pi()/180);
            RL_cl((i-1)*seg_step+j+1,3)= RL_cl((i-1)*seg_step+j,3)+RL(i,3)/...
                seg_step;
        elseif RL(i)==-1     % right turn case
            RL_cl((i-1)*seg_step+j+1,1)= RL_cl((i-1)*seg_step+j,1)+...
                RL(i,2)*sqrt(2-2*cos(RL(i,3)/seg_step*pi()/180))*...
                cos((RL_cl((i-1)*seg_step+j,3)-RL(i,3)/seg_step/2)*pi()/180);
            RL_cl((i-1)*seg_step+j+1,2)= RL_cl((i-1)*seg_step+j,2)+...
                RL(i,2)*sqrt(2-2*cos(RL(i,3)/seg_step*pi()/180))*...
                sin((RL_cl((i-1)*seg_step+j,3)-RL(i,3)/seg_step/2)*pi()/180);
            RL_cl((i-1)*seg_step+j+1,3)= RL_cl((i-1)*seg_step+j,3)-RL(i,3)/...
                seg_step;
        end
    end
end

figure(1)
plot(RL_cl(:,1),RL_cl(:,2),'r-.')
hold off

end